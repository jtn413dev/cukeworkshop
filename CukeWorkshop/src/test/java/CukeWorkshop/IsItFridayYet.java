//package CukeWorkshop;
//
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//import static org.junit.Assert.assertEquals;
//
//public class IsItFridayYet {
//	
//	private String today;
//	private String actualAnswer;
//	
//	static String IsItFriday(String today){
//		if (today.equals("Friday")){
//			return "TGIF";
//		}
//		return "Nope";
//	}
//	
//	@Given("today is (.*)$")
//	public void today_is_what_day(String day){
//		this.today = day;
//	}
//	
//	@When("^I ask whether it's Friday yet$")
//	public void i_ask_whether_it_is_Friday_yet(){
//		this.actualAnswer = IsItFriday(today);
//	}
//	
//	@Then("^I should be told \"([^\"]*)\"$")
//	public void i_should_be_told(String expectedAnswer){
//		assertEquals(expectedAnswer, actualAnswer);
//	}
//}